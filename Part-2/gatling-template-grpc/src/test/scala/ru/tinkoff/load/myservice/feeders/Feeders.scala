package ru.tinkoff.load.myservice.feeders

import ru.tinkoff.gatling.feeders._
import io.gatling.core.Predef._

object Feeders {

  val randomRangeString =
    RandomRangeStringFeeder("randomHex", 32, 33, "ABCDEF123456789")

  val sequentialInt = SequentialFeeder("sequentialInt", 11, 1)

  val resourcesUrl = csv("pools/resourcesUrl.csv").random
}
