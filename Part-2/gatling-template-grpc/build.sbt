import Dependencies._

enablePlugins(GatlingPlugin)
inConfig(Test)(sbtprotoc.ProtocPlugin.protobufConfigSettings)

PB.targets in Test := Seq(
  scalapb.gen() -> (sourceManaged in Test).value
)

lazy val root = (project in file("."))
  .settings(
    inThisBuild(List(
      organization := "ru.tinkoff.load",
      scalaVersion := "2.12.12",
      version := "0.1.0"
    )),
    name := "myservice",
    libraryDependencies ++= gatling,
    libraryDependencies ++= gelf,
    libraryDependencies ++= gatlingPicatiny,
    libraryDependencies ++= janino,
    libraryDependencies ++= gatlingGRPC,
    libraryDependencies ++= grpcDeps,
    scalacOptions ++= Seq(
      "-encoding", "UTF-8", // Option and arguments on same line
      "-Xfatal-warnings",  // New lines for each options
      "-deprecation",
      "-feature",
      "-unchecked",
      "-language:implicitConversions",
      "-language:higherKinds",
      "-language:existentials",
      "-language:postfixOps"
    )
  )
