package ru.tinkoff.load.myservice.cases

import io.gatling.core.Predef._
import ru.tinkoff.load.jdbc.actions
import ru.tinkoff.load.jdbc.Predef._
import ru.tinkoff.load.jdbc.actions.Columns

object Actions {

  def insertTableDebit(tableName: String): actions.DBInsertActionBuilder =
    jdbc("INSERT debit" + tableName)
      .insertInto(tableName, Columns( "Account", "Amount", "TypeOperation"))
      .values(
        "Account" -> "${Account}",
        "Amount"   -> "${Amount}",
        "TypeOperation"        -> "Debit"
      )

  def insertTableCredit(tableName: String): actions.DBInsertActionBuilder =
    jdbc("INSERT credit" + tableName)
      .insertInto(tableName, Columns( "Account", "Amount", "TypeOperation"))
      .values(
        "Account" -> "${Account}",
        "Amount"   -> "${Amount}",
        "TypeOperation"        -> "Credit"
      )

  def updateTable(tableName: String): actions.RawSqlActionBuilder = {
    jdbc(s"UPDATE $tableName ").rawSql(s"UPDATE $tableName SET amount = 1 WHERE TypeOperation = 'Debit' and amount = 1000")
  }

  def updateTableAll(tableName: String): actions.RawSqlActionBuilder = {
    jdbc(s"UPDATE All $tableName ").rawSql(s"UPDATE $tableName SET amount = 1000 ")
  }

  def selectTable(tableName: String): actions.RawSqlActionBuilder = {
    jdbc(s"SELECT $tableName").rawSql(s"SELECT * FROM $tableName")
  }

  def deleteTable(tableName: String): actions.RawSqlActionBuilder = {
      jdbc(s"Clean table $tableName").rawSql(s"delete from $tableName")
  }

}
