# Подготовка

## Содержание

* [Установка Docker](#установка-docker)
* [Загрузка образов Docker](#загрузка-образов-docker)
* [Установка SBT](#установка-sbt)
* [Установка IntellijIDEA и Scala plugin](#установка-intellijidea-и-scala-plugin)
* [Проверка успешной установки](#проверка-успешной-установки)

### Установка Docker

[Общие инструкции по установке docker](https://docs.docker.com/get-docker/)

<details>
<summary>Как установить Docker на Windows</summary>

* Рекомендуется [установить WSL 2.0](https://docs.microsoft.com/en-us/windows/wsl/install-win10) и далее следовать инструкции по установке docker для linux
* Либо [установить docker для Windows](https://docs.docker.com/docker-for-windows/install/). Не подходит для Windows Home, в остальных случаях могут возникать проблемы с работой.

</details>

<details>
<summary>Как установить Docker на Linux/WSL 2.0</summary>

Выполните действия из инструкций последовательно:
 * [Установить docker](https://docs.docker.com/engine/install/ubuntu/)
 * [Выполнять команды docker без sudo](https://docs.docker.com/engine/install/linux-postinstall/)
 * [Установить docker-compose](https://docs.docker.com/compose/install/). Обратите внимание, docker-compose устанавливается отдельно.

</details>

<details>
<summary>Как установить Docker на Mac</summary>

* Следовать [инструкции](https://docs.docker.com/docker-for-mac/install/)

</details>

### Загрузка образов Docker

* Скачать образы для окружения:

```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox.git
cd gatling-sandbox
docker-compose pull
```

* Скачать образ для запуска тестов:

```shell script
docker pull hseeberger/scala-sbt:11.0.8_1.3.13_2.12.12
```

### Установка SBT

**Примечание**:
Воркшоп предусматривает работу без установки SBT, с использованем готовых материалов.
Если не получилось установить SBT, можно отложить этот шаг до начала самостоятельной работы с Gatling.
Также можно использовать в работе [другую систему сборки](https://gatling.io/docs/current/installation/#using-a-build-tool).

* Для работы SBT требуется Java JDK, [скачать и установить можно по ссылке](https://adoptopenjdk.net/installation.html), следуя инструкции для своей платформы. Рекомендуется версия JDK 11.
* Общая инструкция по установке SBT [доступна по ссылке](https://www.scala-sbt.org/1.x/docs/Setup.html), [скачать SBT для Windows](https://www.scala-sbt.org/download.html). Рекомендуется версия SBT >1.3.9.

### Установка IntellijIDEA и Scala plugin
**Примечание**:
Воркшоп предусматривает работу без установки IntellijIDEA и Scala plugin, можно использовать любой текстовый редактор.
IDEA рекомендуется для дальнейшей самостоятельной работы с Gatling.

* [Скачать и установить IntellijIDEA Community](https://www.jetbrains.com/ru-ru/idea/download/).
* [Установить Scala plugin](https://plugins.jetbrains.com/plugin/1347-scala). [Как установить plugin в IntellijIDEA](https://www.jetbrains.com/help/idea/managing-plugins.html#repos)

### Проверка успешной установки

<details>
<summary>Проверить что Docker и Docker-compose успешно установлены</summary>

```shell script
docker --version
>Docker version 19.03.13, build 4484c46d9d
docker-compose --version
>docker-compose version 1.27.4, build 40524192
```

При возникновении проблем:

Linux:
Проверить что docker запущен (зависит от OS и настроек).

Вариант 1:
```shell script
sudo service docker status # проверить статус
sudo service docker start # запустить процесс
```
Варинат 2:
```shell script
sudo systemctl status docker # проверить статус
sudo systemctl start docker # запустить процесс
```

Windows (без WSL 2.0):
Проверить в трее статус сервиса, запустить/перезапустить в случае необходимости.

</details>

<details>
<summary>Проверить что все контейнеры загружены и готовы к работе</summary>

Выполнить в консоли:

```shell script
docker images
>REPOSITORY                          TAG                     IMAGE ID            CREATED             SIZE
>prom/prometheus                     v2.22.0                 7adf5a25557b        7 days ago          168MB
>influxdb                            1.8                     c15aefdd926b        9 days ago          307MB
>grafana/grafana                     7.2.1                   2f17bd84b75d        2 weeks ago         180MB
>hseeberger/scala-sbt                11.0.8_1.3.13_2.12.12   04ad0fd234ec        2 weeks ago         990MB
>gitlab/gitlab-runner                v13.4.1                 c12323e3103b        3 weeks ago         483MB
>portainer/portainer                 1.24.1                  62771b0b9b09        3 months ago        79.1MB
>timberio/vector                     0.10.0-alpine           2ebd263245dd        3 months ago        80.7MB
>grafana/loki                        1.5.0                   8646eadeb75e        5 months ago        183MB
>gcr.io/google-containers/cadvisor   v0.34.0                 d24b7db72c99        14 months ago       185MB
```

</details>

<details>
<summary>Проверить что Java установлена и работает</summary>

Подходит любая версия Java 11 версии:
```shell script
java --version
>openjdk 11.0.9 2020-10-20
>OpenJDK Runtime Environment (build 11.0.9+11-Ubuntu-0ubuntu1.20.04)
>OpenJDK 64-Bit Server VM (build 11.0.9+11-Ubuntu-0ubuntu1.20.04, mixed mode, sharing)
```

</details>

<details>
<summary>Проверить что SBT установлена и работает</summary>

```shell script
sbt --version
>sbt version in this project: 1.3.9
>sbt script version: 1.3.9
```

</details>
